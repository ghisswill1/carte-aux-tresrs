package com.carteauxtresors.service;

import com.carteauxtresors.model.Aventurier;
import com.carteauxtresors.model.Carte;
import com.carteauxtresors.model.Montagne;
import com.carteauxtresors.model.Tresor;

import java.util.List;

public interface GameService {
    String runGame(Carte carte, List<Montagne> montagnes, List<Tresor> tresors, List<Aventurier> aventurier);
}
