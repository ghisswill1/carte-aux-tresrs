package com.carteauxtresors.service;

import com.carteauxtresors.model.Aventurier;
import com.carteauxtresors.model.Carte;
import com.carteauxtresors.model.Montagne;
import com.carteauxtresors.model.Tresor;

import java.io.IOException;
import java.util.List;

public interface CarteTresorService {

    String getResources() throws IOException;

    List<Carte> getCarteFromLine(List<String> cartes);

    List<Montagne> getMontagneFromLine(List<String> montagnesString);

    List<Tresor> getTresorFromLine(List<String> tresorsString);

    List<Aventurier> getAventurierFromLine(List<String> aventuriersString);
}
