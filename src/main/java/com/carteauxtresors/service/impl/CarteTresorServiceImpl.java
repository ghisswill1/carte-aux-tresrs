package com.carteauxtresors.service.impl;

import com.carteauxtresors.model.*;
import com.carteauxtresors.service.CarteTresorService;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarteTresorServiceImpl implements CarteTresorService {

    public String getResources() throws IOException {
        try (InputStream inputStream = getClass().getResourceAsStream("/resources.txt");
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            return reader.lines()
                    .collect(Collectors.joining(System.lineSeparator()));
        }
    }

    public List<Carte> getCarteFromLine(List<String> cartesString) {
        List<Carte> cartes = new ArrayList<>();
        cartesString.forEach(carteString -> {
            String[] carteArray = carteString.split("-");
            carteArray = Arrays.copyOfRange(carteArray, 1, carteArray.length);
            cartes.add(new Carte(Integer.parseInt(carteArray[0].trim()), Integer.parseInt(carteArray[1].trim())));
        });
        return cartes;
    }

    public List<Montagne> getMontagneFromLine(List<String> montagnesString) {
        List<Montagne> montagnes = new ArrayList<>();

        montagnesString.forEach(montagneString -> {
            String[] montagneArray = montagneString.split("-");
            montagneArray = Arrays.copyOfRange(montagneArray, 1, montagneArray.length);
            var postion = Position.builder()
                    .x(Integer.parseInt(montagneArray[1].trim()))
                    .y(Integer.parseInt(montagneArray[1].trim()))
                    .build();
            montagnes.add(new Montagne(postion));
        });
        return montagnes;
    }

    public List<Tresor> getTresorFromLine(List<String> tresorsString) {
        List<Tresor> tresors = new ArrayList<>();

        tresorsString.forEach(tresorString -> {
            String[] tresorsArray = tresorString.split("-");
            tresorsArray = Arrays.copyOfRange(tresorsArray, 1, tresorsArray.length);
            var position = Position.builder()
                    .x(Integer.parseInt(tresorsArray[0].trim()))
                    .y(Integer.parseInt(tresorsArray[1].trim()))
                    .build();
            tresors.add(new Tresor(position, Integer.parseInt(tresorsArray[2].trim())));
        });
        return tresors;
    }

    public List<Aventurier> getAventurierFromLine(List<String> aventuriersString) {
        List<Aventurier> aventuriers = new ArrayList<>();
        aventuriersString.forEach(aventurierString -> {
            String[] aventurierArray = aventurierString.split("-");
            aventurierArray = Arrays.copyOfRange(aventurierArray, 1, aventurierArray.length);
            var position = Position.builder()
                    .x(Integer.parseInt(aventurierArray[1].trim()))
                    .y(Integer.parseInt(aventurierArray[2].trim()))
                    .build();
            aventuriers.add(new Aventurier(aventurierArray[0].trim(),position
                    ,aventurierArray[3].trim().charAt(0), aventurierArray[4].trim(), 0));
        });
        return aventuriers;
    }
}
