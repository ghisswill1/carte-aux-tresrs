package com.carteauxtresors.service.impl;

import com.carteauxtresors.model.*;
import com.carteauxtresors.service.GameService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class GameServiceImpl implements GameService {

    @Override
    public String runGame(Carte carte, List<Montagne> montagnes, List<Tresor> tresors, List<Aventurier> aventuriers) {
        String resume = "";
        int lognestSequence = getLongestSequence(aventuriers);
        for (int i = 0; i < lognestSequence; i++) {
            for (int j = 0; j < aventuriers.size(); j++) {
                Aventurier aventurier = aventuriers.get(j);
                if (aventurier.getSequenceMvt().length() <= i) {
                    break;
                }
            char mvtCourant = aventurier.getSequenceMvt().charAt(i);
            var dir = aventurier.getOrientation();
            switch (mvtCourant) {
                case 'D':
                    dir = getDirection(dir, true);
                    aventurier.setOrientation(dir);
                    break;
                case 'G':
                    dir = getDirection(dir, false);
                    aventurier.setOrientation(dir);
                    break;
                case 'A':
                    boolean mvtOk = true;
                    boolean isYaxisMvt = true;
                    PosMvtInfo posMvtInfo = expectedMvt(aventurier, isYaxisMvt);

                    Position nextPosition = Position.builder()
                            .x(posMvtInfo.position.getX())
                            .y(posMvtInfo.position.getY()).build();
                    // rencontre Montage
                    for(Montagne m : montagnes) {
                        if(m.getPosition().equals(nextPosition)) {
                            mvtOk = false;
                            break;
                        }
                    }
                    // deplcement hors de la carte
                    if (!posMvtInfo.isYaxisMvt && nextPosition.getX() > carte.getNbreCaseLargeur() && nextPosition.getX() < 0){
                        mvtOk = false;
                    }
                    if(posMvtInfo.isYaxisMvt && nextPosition.getY() < 0 && nextPosition.getY() > carte.getNbreCaseHauteur()) {
                        mvtOk = false;
                    }
                    // Collision aventurier
                    for (Aventurier autreAventurier : aventuriers) {
                        if(!autreAventurier.equals(aventurier)) {
                            if(autreAventurier.getPosition().equals(nextPosition)) {
                                mvtOk = false;
                                break;
                            }
                        }
                    }
                    // deplement aventurier
                    if (mvtOk) {
                        aventurier.setPosition(nextPosition);
                        collecteTresor(tresors, aventurier);
                    }
                    break;
                default:
                    break;
            }
            }
        }
        for (Aventurier aventurier : aventuriers) {
             resume += aventurier.getName() + " collecte " + aventurier.getTresorCollecte() +
                    " trésors et finit son parcours en ( " + aventurier.getPosition().getX() + "  - " +  aventurier.getPosition().getY() + " )\n";
        }
        return resume;
    }

    private int getLongestSequence(List<Aventurier> aventuriers) {
        AtomicInteger maxLength = new AtomicInteger();
        aventuriers.forEach(aventurier -> {
            if(aventurier.getSequenceMvt().length() > maxLength.get())
                maxLength.set(aventurier.getSequenceMvt().length());
        });
        return maxLength.get();
    }

    private static char getDirection(char dir, boolean isClockWise) {
        switch (dir) {
            case 'N':
                dir = isClockWise ? 'E' : 'O';
                break;
            case 'S':
                dir = isClockWise ?'O' : 'E';
                break;
            case 'E':
                dir = isClockWise ? 'S' : 'N';
                break;
            case 'O':
                dir = isClockWise ? 'N' : 'S';
                break;
            default:
                break;
        }
        return dir;
    }

    private void collecteTresor(List<Tresor> tresors, Aventurier aventurier) {
        List<Tresor> aSupprimerList = new ArrayList<>();
        for (Tresor tresor : tresors) {
            if(tresor.getPosition().equals(aventurier.getPosition())) {
                aventurier.setTresorCollecte(aventurier.getTresorCollecte() + 1);

                if(tresor.getNbreTresors() > 0) {
                    tresor.setNbreTresors(tresor.getNbreTresors() - 1);
                } else {
                    aSupprimerList.add(tresor);
                }
            }
        }
        if(!aSupprimerList.isEmpty() )
            tresors.removeAll(aSupprimerList);
    }

    private PosMvtInfo expectedMvt(Aventurier aventurier, boolean isYaxisMvt) {
        int x = aventurier.getPosition().getX();
        int y = aventurier.getPosition().getY();
        switch (aventurier.getOrientation()) {
            case 'N':
                y--;
                break;
            case 'S':
                y++;
                break;
            case 'E':
                x++;
                isYaxisMvt = false;
                break;
            case 'O':
                x--;
                isYaxisMvt = false;
                break;
            default:
                break;
        }

        return new PosMvtInfo(new Position(x,y), isYaxisMvt);
    }

    private record PosMvtInfo(Position position, boolean isYaxisMvt) { }
}
