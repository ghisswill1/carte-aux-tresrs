package com.carteauxtresors.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Carte {
    private int nbreCaseLargeur;
    private int nbreCaseHauteur;
}
