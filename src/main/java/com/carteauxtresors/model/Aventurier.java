package com.carteauxtresors.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Aventurier {
    private String name;
    private Position position;
    private char orientation;
    private String sequenceMvt;
    private int tresorCollecte;
}
