package com.carteauxtresors.model;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Tresor extends Montagne{
    private int nbreTresors;

    public Tresor(Position position, int nbreTresor) {
        super(position);
        this.nbreTresors = nbreTresor;
    }
}
