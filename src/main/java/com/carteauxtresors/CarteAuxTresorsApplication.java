package com.carteauxtresors;

import com.carteauxtresors.model.Aventurier;
import com.carteauxtresors.model.Carte;
import com.carteauxtresors.model.Montagne;
import com.carteauxtresors.model.Tresor;
import com.carteauxtresors.service.GameService;
import com.carteauxtresors.service.CarteTresorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ResourceLoader;

import java.util.List;

@SpringBootApplication
public class CarteAuxTresorsApplication implements CommandLineRunner {

	@Autowired
	ResourceLoader resourceLoader;
	@Autowired
	CarteTresorService carteTresorService;

	@Autowired
	GameService gameService;

	public static void main(String[] args) {
		SpringApplication.run(CarteAuxTresorsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		String contents = carteTresorService.getResources();

		List<String> cartesString = contents.lines().filter(e->e.contains("C")).toList();
		List<Carte> cartes = carteTresorService.getCarteFromLine(cartesString);

		List<String> montagnesString = contents.lines().filter(e->e.contains("M")).toList();
		List<Montagne> montagnes = carteTresorService.getMontagneFromLine(montagnesString);

		List<String> tresorsString = contents.lines().filter(e->e.contains("T")).toList();
		List<Tresor> tresors = carteTresorService.getTresorFromLine(tresorsString);

		List<String> aventuriersString = contents.lines().filter(e->e.contains("A")).toList();
		List<Aventurier> aventuriers = carteTresorService.getAventurierFromLine(aventuriersString);


		String res = gameService.runGame(cartes.get(0), montagnes, tresors, aventuriers);

		System.out.println();
		System.out.println("**************************************************************");
		System.out.println(res);
	}
}
