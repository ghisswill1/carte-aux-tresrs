package com.carteauxtresors.service;

import com.carteauxtresors.model.Aventurier;
import com.carteauxtresors.model.Carte;
import com.carteauxtresors.model.Tresor;
import com.carteauxtresors.service.impl.CarteTresorServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
public class CarteTresorServiceTest {

    @InjectMocks
    CarteTresorServiceImpl carteTresorService;

    @Test
    public void whenFile_thenGetResources() throws IOException {
        String sourceUrl = "/resources.txt";
        String resourcesString = loaderResource(sourceUrl);

        String res = carteTresorService.getResources();

        Assertions.assertThat(res).isNotEmpty();
        Assertions.assertThat(resourcesString).isNotEmpty();
        Assertions.assertThat(res).isEqualTo(resourcesString);
    }

    @Test
    public void whenFile_thenGetCarteFromLine() throws IOException {
        String sourceUrl = "/resources.txt";
        String resourcesString = loaderResource(sourceUrl);
        List<String> cartesString = resourcesString.lines().filter(e->e.contains("C")).toList();

        List<Carte> cartes = carteTresorService.getCarteFromLine(cartesString);

        Assertions.assertThat(cartes.size()).isEqualTo(cartesString.size());
        Assertions.assertThat(cartes.get(0).getNbreCaseHauteur()).isEqualTo(4);
        Assertions.assertThat(cartes.get(0).getNbreCaseLargeur()).isEqualTo(3);
    }

    @Test
    public void whenFile_thenGetMontagneFromLine() throws IOException {
        String sourceUrl = "/resources.txt";
        String resourcesString = loaderResource(sourceUrl);
        List<String> cartesString = resourcesString.lines().filter(e->e.contains("C")).toList();

        List<Carte> cartes = carteTresorService.getCarteFromLine(cartesString);

        Assertions.assertThat(cartes.size()).isEqualTo(cartesString.size());
    }

    @Test
    public void whenFile_thenGetTresorFromLine() throws IOException {
        String sourceUrl = "/resources.txt";
        String resourcesString = loaderResource(sourceUrl);
        List<String> tresorsString = resourcesString.lines().filter(e->e.contains("T")).toList();

        List<Tresor> tresors = carteTresorService.getTresorFromLine(tresorsString);

        Assertions.assertThat(tresors.size()).isEqualTo(tresorsString.size());
        Assertions.assertThat(tresors.get(0).getNbreTresors()).isEqualTo(2);
    }

    @Test
    public void whenFile_thenGetAventurierFromLine() throws IOException {
        String sourceUrl = "/resources.txt";
        String resourcesString = loaderResource(sourceUrl);
        List<String> aventuriersString = resourcesString.lines().filter(e->e.contains("A")).toList();

        List<Aventurier> aventuriers = carteTresorService.getAventurierFromLine(aventuriersString);

        Assertions.assertThat(aventuriers.size()).isEqualTo(aventuriersString.size());
        Assertions.assertThat(aventuriers.get(0).toString()).isEqualTo(aventuriers.get(0).toString());
    }

    private String loaderResource(String sourceUrl) throws IOException {
        try (InputStream inputStream = getClass().getResourceAsStream(sourceUrl);
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            return reader.lines()
                    .collect(Collectors.joining(System.lineSeparator()));
        }
    }
}
