package com.carteauxtresors.service;

import com.carteauxtresors.model.*;
import com.carteauxtresors.service.impl.GameServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
public class GameServiceTest {

    @InjectMocks
    GameServiceImpl gameService;

    @Test
    public void whenResource_thenRunGame(){
        Carte carte = new Carte(3,4);
        Montagne montagne = new Montagne(new Position(1,0));
        Tresor tresor = new Tresor(new Position(0,3),2);
        Aventurier aventurier = new Aventurier("Jean", new Position(1,1), 'S', "AADADAGGA", 0);

        String expected = "Jean collecte 2 trésors et finit son parcours en ( 0  - 3 )\n";

        String res = gameService.runGame(carte, List.of(montagne), List.of(tresor), List.of(aventurier));

        Assertions.assertThat(res).isNotEmpty();
        Assertions.assertThat(res).isEqualTo(expected);
    }

    @Test
    public void whenResourceWithTwoAventuriers_thenRunGame(){
        Carte carte = new Carte(3,4);
        Montagne montagne = new Montagne(new Position(1,0));
        Tresor tresor = new Tresor(new Position(0,3),2);
        Aventurier aventurier = new Aventurier("Jean", new Position(1,1), 'S', "AADADAGGA", 0);
        Aventurier aventurier1 = new Aventurier("Jean", new Position(2,1), 'S', "AADADAGGA", 0);

        String expected = "Jean collecte 2 trésors et finit son parcours en ( 0  - 3 )\n" +
                "Jean collecte 0 trésors et finit son parcours en ( 1  - 3 )\n";

        String res = gameService.runGame(carte, List.of(montagne), List.of(tresor), List.of(aventurier, aventurier1));

        Assertions.assertThat(res).isNotEmpty();
        Assertions.assertThat(res).isEqualTo(expected);
    }
}
